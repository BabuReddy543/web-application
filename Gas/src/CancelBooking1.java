

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class CancelBooking1
 */
@WebServlet("/CancelBooking1")
public class CancelBooking1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private int custId;
	private int Booking_No;
	private int CUSTOMER_ID;
	private Date BOOK_DATE;
	private Date DELIVERY_DATE;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CancelBooking1() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter pw = response.getWriter();
		HttpSession session = request.getSession(false);

		try
		{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			java.sql.Connection cn;
			cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "book", "babu");
			System.out.println("in try");
			HttpSession s = request.getSession(false);
			custId = (int) session.getAttribute("custId");
			PreparedStatement ps = cn.prepareStatement("Update Booking set CUSTOMER_ID=?,BOOK_DATE=?,DELIVERY_DATE=?,STATUS=? where CUSTOMER_ID= "+custId);
			ps.setInt(1,custId);
			ps.setDate(2,BOOK_DATE);
			ps.setDate(3,DELIVERY_DATE);
			ps.setString(4,"C");
			
			int i = ps.executeUpdate();
			if (i != 0) {
				response.sendRedirect("CancelView.jsp");
			}
			pw.close();
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
