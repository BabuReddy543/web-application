package controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Service.Dbconnection;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private int custId;
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Login() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		HttpSession s = request.getSession(); 
		HttpSession session = request.getSession(false); 
		try {
			Connection con = Dbconnection.getConnection();	
			java.sql.Statement st = con.createStatement();
			java.sql.Statement st1 = con.createStatement();
			String UserName = request.getParameter("UserName");
			String Password = request.getParameter("Password");
			String radio = request.getParameter("radio");
		    String str;
		    ResultSet rs,rs1;
		    s.setAttribute("custId", custId);
		   session.setAttribute("CusName", UserName);
		    
		    if(radio.equals("User"))
		    {
		    System.out.println("in if");
		str = "select * from Customer where CUSTOMER_NAME= '" + UserName + "' and PASSWORD='" + Password+ "'";
		rs = st.executeQuery(str);
		if (rs.next())
		{

		response.sendRedirect("Success.jsp");
		}
		else {
		response.sendRedirect("Login.jsp");
		}
		    }
		else if(radio.equals("Admin"))
		    {
		    System.out.println("in else");
		    str = "select * from Distributor where DISTRIBUTOR_NAME= '" + UserName + "' and PASSWORD='" + Password+ "'";
		    System.out.println(str);
		    rs1 = st1.executeQuery(str);
		System.out.print("after execute");
		if (rs1.next())
		{
		System.out.println("in sucess");
		response.sendRedirect("DistributorView.jsp");
		}
		else
		{
		System.out.println("in failure");
		response.sendRedirect("Login.jsp");
		}    
		    }
			
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}
}
