
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sun.org.apache.xerces.internal.util.Status;

/**
 * Servlet implementation class BookServlet
 */
@WebServlet("/BookServlet")
public class BookServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private int CUSTOMER_ID;
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public BookServlet() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter pw = response.getWriter();
		HttpSession s = request.getSession(false);
		
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			java.sql.Connection cn;
			cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "book", "babu");
			System.out.println("in try");
			long milliseconds = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(milliseconds);
			System.out.println(date);

			System.out.println("new...");
			Calendar calender = Calendar.getInstance();
			calender.setTime(date);
			calender.add(Calendar.DATE, 5);
			java.sql.Date dates = new java.sql.Date(calender.getTimeInMillis());
			
			
			System.out.println(" after 5 days" + dates);
			PreparedStatement st = cn.prepareStatement("insert into Booking values(BookingSeq.nextval,?,?,?,?)");
			st.setInt(1, CUSTOMER_ID);
			st.setDate(2, date);
			st.setDate(3, dates);
			st.setString(4, "B");
			int i = st.executeUpdate();
			if (i != 0) {
				response.sendRedirect("BookView.jsp");
			}
			pw.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

}
